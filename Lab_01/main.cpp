#include <iostream>
using namespace std;

int sum(int*, int);// calculates summation of inputs
float avg(int*, int);// calculates average of inputs  
int product(int*, int);// calculates product of inputs
int smallest(int*, int);// calculates smallest of inputs

int main()
{
	int* inputs;// holds all integer inputs
	int num_of_in;// holds number of inputs
	FILE* in_file;
	fopen_s(&in_file, "input.txt", "r"); // opening .txt file 
	if (in_file == NULL)// if the file does not open
	{
		printf("\nFile is not available.\n");
		return 0;// exit the program
	}

	fscanf_s(in_file, "%d", &num_of_in); // taking first integer as the number of integers
	inputs = (int*)malloc(num_of_in * sizeof(int));// allocating memory for the input data asmucch as needed

	char trial_char; // if next char is not '\n' than file formatted wrong
	fscanf_s(in_file, "%c", &trial_char, sizeof(char));
	if (trial_char != '\n'){
		printf("\nError! Writing format is unvalid.\n");
		return 0;
	}

	int trial_int = NULL, trial_int_temp = NULL;
	for (int i = 0; i < num_of_in; i++) // filling 'inputs' array one by one
	{
		trial_int = *(inputs + i);
		fscanf_s(in_file, "%d", inputs + i);
		if (*(inputs + i) == trial_int) // if the value does not change there is integer(s) missing in txt file
		{
			printf("\nError! Integer missing.\n");
			return 0;
		}
	}
	// if the value changes there is more integer values written in txt file
	trial_int_temp = trial_int;
	fscanf_s(in_file, "%d", &trial_int);
	if (trial_int != trial_int_temp)
	{
		printf("\nError! More integer than expected.\n");
		return 0;
	}
	fclose(in_file);// closing file

	//printing output
	cout << "\nSum: " << sum(inputs, num_of_in) << "\nAvg: " << avg(inputs, num_of_in) << "\nProduct: "
		<< product(inputs, num_of_in) << "\nSmallest: " << smallest(inputs, num_of_in) << "\n\n";

	system("pause");
}

int sum(int* arr, int num_of_elements)
{
	int sum = 0;
	for (int i = 0; i < num_of_elements; i++)
	{
		sum += *(arr + i);
	}
	return sum;
}

float avg(int* arr, int num_of_elements)
{
	float sum_f = sum(arr, num_of_elements);
	return sum_f / num_of_elements;
}

int product(int* arr, int num_of_elements)
{
	long int prod = 1;
	for (int i = 0; i < num_of_elements; i++)
	{
		prod *= *(arr + i);
	}
	return prod;
}

int smallest(int* arr, int num_of_elements)
{
	int smallest_f = *(arr);
	for (int i = 1; i < num_of_elements; i++)
	{
		if (*(arr + i) < smallest_f)
			smallest_f = *(arr + i);
	}
	return smallest_f;
}