#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int size;
    cin >> size;
    int * arr = new int[size];
    int i = 0;
    while(i < size){
        scanf("%d", arr + i);
        i++;
    }
    for(i--; i >= 0; i--)
    {
        cout << *(arr + i) << ' ';
    }
    return 0;
}
