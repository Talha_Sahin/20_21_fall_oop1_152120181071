#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool isok(string, string*, int);

int main() {
    int num_of_lines, num_of_query;
    scanf("%d", &num_of_lines);
    scanf("%d", &num_of_query);
    cin.ignore();

    char bf[200];
    string* all = new string[num_of_lines];
    for (int i = 0; i < num_of_lines; i++)
    {
        fgets(bf, 200, stdin);
        *(all + i) = bf;
    }

    string* allq = new string[num_of_lines];
    for (int i = 0; i < num_of_query; i++)
    {
        fgets(bf, 200, stdin);
        *(allq + i) = bf;
    }

    char wanted[200], wnttmp[200], tagtmp[200];
    bool flag, wntflag;
    int temp_index;
    string* taglist;
    for (int i = 0; i < num_of_query; i++)
    {
        flag = 0;
        for (int j = 0; ; j++)
        {
            if (allq[i][j] == '\0') {
                if (wanted[j - temp_index - 1] != '\n') {
                    wanted[j - temp_index] = '\n';
                    wanted[j - temp_index + 1] = '\0';
                }
                else {
                    wanted[j - temp_index] = '\0';
                }
                break;
            }

            if (flag == 1)
                wanted[j - temp_index] = allq[i][j];

            if (allq[i][j] == '~') {
                flag = 1;
                temp_index = j + 1;
            }

        }
        wntflag = 0;
        taglist = new string[num_of_lines];
        for (int j = 0; j < num_of_lines; j++)
        {
            int z;
            for (z = 1; ; z++)
            {
                if (all[j][z] == ' ' || all[j][z] == '>')
                    break;
                if (all[j][1] != '/')
                {
                    bf[z - 1] = all[j][z];
                }
                else
                {
                    if (all[j][z + 1] == ' ' || all[j][z + 1] == '>')
                        break;
                    bf[z - 1] = all[j][z + 1];
                }
            }
            bf[z - 1] = '\0';
            taglist[j] = bf;
            flag = 0;
            for (int k = 0; ; k++)
            {
                if (flag == 1)
                    wnttmp[k - temp_index] = all[j][k];

                if (all[j][k] == ' ' || all[j][k] == '=')
                {
                    if (flag == 0) {
                        flag = 1;
                        temp_index = k + 1;
                    }
                    else
                    {
                        
                        flag = 0;
                        bool spaceflag = 0;
                        wnttmp[k - temp_index] = '\n';
                        wnttmp[k - temp_index + 1] = '\0';
                        if (string(wnttmp) == string(wanted))
                        {
                            if (isok(allq[i], taglist, j) == 1)
                            {
                                for (int o = 0; ; o++)
                                {
                                    if (all[j][k + o] == '"'){
                                        if (spaceflag == 0) {
                                            spaceflag = 1;
                                            o++;
                                        }
                                        else
                                            break;
                                    }
                                    if (spaceflag == 1)
                                        cout << all[j][k + o];
                                    
                                }
                                cout << '\n';
                                wntflag = 1;
                            }
                        }
                    }
                }
                
                if (all[j][k] == '>' || wntflag == 1)
                    break;
            }
            if (wntflag == 1)
                break;
        }
        if(wntflag == 0)
            cout << "Not Found!" << '\n';
    }

    return 0;
}

bool isok(string query, string* tags, int index)
{
    for (int i = 0; i <= index; i++) {
        for (int j = i + 1; j <= index; j++) {
            if (string(tags[i]) == string(tags[j]))
            {
                tags[i][0] = '\0';
                tags[j][0] = '\0';
            }
        }
    }
    char buff[200];
    int qcnt = 0;
    bool flag = 1, tflag = 0;

    for (int i = 0; i <= index; i++)
    {
        buff[0] = '\0';
        if (tags[i][0] != '\0')
        {
            for (int j = 0; ; j++) {

                if (query[qcnt] == '.') {
                    buff[j] = '\0'; qcnt++; break;
                }
                else if (query[qcnt] == '~')
                {
                    tflag = 1;
                    if (index == i) {
                        buff[j] = '\0';
                        break;
                    }
                    else {
                        flag = 0;
                        break;
                    }
                }
                buff[j] = query[qcnt];
                qcnt++;

            }
            if (string(buff) != string(tags[i]))
                flag = 0;
        }
        if (flag == 0)
            break;
    }
    if (tflag == 0)
        flag = 0;
    return flag;
}

