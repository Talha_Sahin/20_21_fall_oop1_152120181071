#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    int a1, a2;
    cin >> a1;
    cin >> a2;
    for(; a1 <= a2; a1++)
    { 
        if(a1 <= 9)
        {
            if(a1 == 1)
                cout << "one\n";
            if(a1 == 2)
                cout << "two\n";
            if(a1 == 3)
                cout << "three\n";
            if(a1 == 4)
                cout << "four\n";
            if(a1 == 5)
                cout << "five\n";
            if(a1 == 6)
                cout << "six\n";
            if(a1 == 7)
                cout << "seven\n";
            if(a1 == 8)
                cout << "eight\n";
            if(a1 == 9)
                cout << "nine\n";
        }
        else 
        {
            if(a1 % 2 == 0)
                cout << "even\n";
            else
                cout << "odd\n"; 
        }
    }
    return 0;
}