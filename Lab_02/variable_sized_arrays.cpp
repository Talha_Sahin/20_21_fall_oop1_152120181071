#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int num_of_array, num_of_query, dim;
    scanf("%d", &num_of_array);
    scanf("%d", &num_of_query);
    
    int ** big = new int*[num_of_array];
    for (int i = 0; i < num_of_array; i++) 
    {
        scanf("%d", &dim);
        big[i] = new int[dim];
        for (int j = 0; j < dim; j++) {
            scanf("%d", big[i] + j);
        } 
    }   
    
    int arr_index, elem_index;
    for (int i = 0; i < num_of_query; i++) {
        scanf("%d", &arr_index);
        scanf("%d", &elem_index);
        cout << *(big[arr_index] + elem_index) << '\n';
    }
       
    return 0;
}