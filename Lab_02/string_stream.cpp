#include <sstream>
#include <vector>
#include <iostream>
using namespace std;

vector<int> parseInts(string str) {
    int numOfComa = 0, temp;
    char dis;
    vector <int> outvec;
    stringstream ss(str);
    for (int i = 0; ; i++) {
        dis = '-';
        ss >> temp >> dis;
        outvec.push_back(temp);
        if(dis != ',')
            break;
    }
    return outvec;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for(int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    
    return 0;
}
